// Rob Johnson - lab 9 - pcc cs162 - 12/4/18

#include <iostream>
#include <cstring>

#define MAX_STRLEN 201

using namespace std;

struct Node
{
    char data[MAX_STRLEN];
    Node *next;
};

void addList(Node *&head, char *val)
{
    Node *temp = head;
    Node *ptr = new Node;
    strcpy(ptr->data, val);
    ptr->next = NULL;

    // test to see if first element of linked list
    if(head != NULL)
    {
        // navigate to the end of the linked list
        while(temp->next != NULL)
        {
           temp = temp->next;
        }

        temp->next = ptr;
    } else {
        head = ptr;
    }
}

void printList(Node *current_node)
{
    // base case - stop at end of list
    if(current_node == NULL)
    {
        return;
    }

    // general case - print data
    cout << current_node->data << endl;
    printList(current_node->next);
}

void isWordInList(Node* current_node, char* word)
{
    if(current_node == NULL)
    {
        cout << "Word NOT found." << endl;
        return;
    }

    if(strcmp(current_node->data, word) == 0)
    {
        cout << "Word found." << endl;
        return;
    }

    isWordInList(current_node->next, word);
}

void deleteList(Node *&head)
{
    while(head != NULL)
    {
        Node* next = head->next;
        delete head;
        head = next;
    }
}

int main(void)
{
    Node *head = NULL;

    cout << "================" << endl;

    char val[MAX_STRLEN];
    do
    {
        cout << "enter data: ";
        cin.getline(val, MAX_STRLEN);
        if(strcmp(val, "quit") != 0)
        {
            addList(head, val);
        }
    } while(strcmp(val, "quit") != 0);

    // print all
    printList(head);


    cout << endl << "word to search for: ";
    cin.getline(val, MAX_STRLEN);
    isWordInList(head, val);

    // delete all
    deleteList(head);

    cout << "================" << endl;

    return 0;
}
